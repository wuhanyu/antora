= Antora Project Roadmap
// Settings:
ifdef::env-browser[]
:toc-title: Contents
:toclevels: 3
:toc:
endif::[]
// Project URIs:
:uri-home: https://antora.org
:uri-org: https://gitlab.com/antora
:uri-repo: {uri-org}/antora
:uri-issues: {uri-repo}/issues
:uri-milestones: {uri-repo}/milestones
:uri-changelog: {uri-repo}/blob/master/CHANGELOG.adoc
:uri-demo-issues: https://gitlab.com/groups/antora/demo/-/issues
:uri-docs-site-issues: {uri-org}/docs.antora.org/issues
:uri-ui-repo: {uri-org}/antora-ui-default
:uri-ui-issues: {uri-ui-repo}/issues

This roadmap provides the current development direction and schedule for {uri-home}[Antora].
It is intended for informational purposes only.
The proposed features, their scope, and the release timeline are not firm commitments.

== Antora Core Components

For a detailed list of current development tasks, refer to the Core components {uri-issues}[issue tracker].

=== v1.1.0

*Release Timeframe*: August 2018

* [x] Make branch for v1.0.x (for docs and patch releases)
* [ ] Support including content from other modules and components using contextual resource ID {uri-issues}/226[#226]
* [ ] Allow a navigation file from another module to be referenced (currently has to be a partial)
* [ ] Implicitly disable branches pattern if tags pattern is specified in content source {uri-issues}/268[#268]
* [ ] Add CLI option to specify a different site generator pipeline {uri-issues}/178[#178]
* [ ] Move partials directory to <module>/partials {uri-issues}/254[#254]
* [ ] Upgrade to Asciidoctor 1.5.7
* [ ] Make components available as a map in the UI model {uri-issues}/253[#253]
* [ ] Make navigation of all component versions accessible from the UI model {uri-issues}/281[#281]
* [ ] Allow pattern for edit URL to be configurable {uri-issues}/283[#283]
* [ ] Make the latest version of a component configurable
* [ ] Test on Node 10 and add Node 10 to CI matrix
* [x] Mark prereleases with next tag and recommended stable version with latest tag {uri-issues}/170[#170]

=== v1.2.0

*Release Timeframe*: Q3 2018

* [ ] Allow AsciiDoc attributes to be specified per component-version (in antora.yml) {uri-issues}/251[#251]
* [ ] Add authentication for remote UI bundles {uri-issues}/280[#280]
* [ ] Replace nodegit with isomorphic-git {uri-issues}/264[#264]
* [ ] Support loading the UI from a directory {uri-issues}/150[#150]
* [ ] Allow content to be included from a URL {uri-issues}/246[#246]
* [ ] Implement fallback value for component version (use git ref) {uri-issues}/161[#161]
* [ ] Navigation model should provide access to prev, next, parent page {uri-issues}/233[#233]
* [ ] Add a logging infrastructure {uri-issues}/145[#145]
//* allow a group to be defined in antora.yml
//* make all metadata from antora.yml available to model
//* allow static files in UI to be decorated with page template / access UI model
//* make module path configurable (using antora.yml) {uri-issues}/28[#28]

=== v1.3.0

*Release Timeframe*: Q4 2018

=== Unscheduled

The capabilities and features in this section have been proposed and tentatively accepted as future work tasks.
They aren't slated for imminent development but are reviewed for possible scheduling after each release.

//* [ ] Pass algolia keys in playbook
* [ ] Set up API documentation and automatically publish as CI artifact
* [ ] Show the file URI where the site can be previewed offline {uri-issues}/220[#220]
* [ ] Generate a robots.txt file (perhaps configured in site.yml) {uri-issues}/219[#219]
* [ ] Add context as second argument to ContentCatalog#addFile {uri-issues}/209[#209]
* [ ] Allow supplemental files to be fetched and added to the content catalog {uri-issues}/195[#195]
* [ ] Add (Apache) httpd redirect facility to redirect producer {uri-issues}/192[#192]
* [ ] Allow static redirect page to be customized using a UI template {uri-issues}/191[#191]
* [ ] Allow page alias to be an explicit URL pathname {uri-issues}/190[#190]
* [ ] Add a merge mode to supplemental UI files {uri-issues}/149[#149]
* [ ] Allow a component to be promoted to the site root {uri-issues}/132[#132]
* [ ] Decide whether content aggregate should be sorted {uri-issues}/121[#121]
* [ ] Ignore duplicate component in same repository if it matches component in HEAD {uri-issues}/120[#120]
* [ ] Set up UI acceptance test suite {uri-issues}/95[#95]
* [ ] Separate content aggregator from git provider {uri-issues}/93[#93]
* [ ] Add option to playbook to skip/bypass worktree(s) in local repositories {uri-issues}/82[#82]
* [ ] Allow module paths to be configurable {uri-issues}/28[#28]
* [ ] Add support for a moduleless docs component {uri-issues}/27[#27]
* Upgrade build to Gulp 4
* Set up webhooks between repositories (e.g., docs.antora.org, docker-antora)
* Component to host mapping, not just one site URL
* Custom UI output dir missing test in generator
* Decide whether to isolate id (or ctx) from src property on content file
* Be able to make references to page aliases; would require parsing all document headers in a separate step/phase
//whiteboard
* contentCatalog.resolvePage()
//whiteboard
* antora-version
* Separate site publisher from providers
* Evaluate new strategies for interpreting equations (e.g., build-time conversion to SVG)
* Watch mode for files in worktree

.Accepted Ideas
* Add support for git-lfs for assets storage such as images (Requirements: {uri-issues}/185[#185])

.Discussions
* Properly store generated PlantUML images directly in Antora content folder instead of output directory (Requirements: {uri-issues}/189[#189])

== Antora Documentation, Demo, & Docs Site

For current Antora documentation tasks, see the Core components {uri-issues}[issue tracker].

For current demo tasks, see the Demo materials {uri-demo-issues}[issue tracker].

For current docs.antora.org tasks, see the site {uri-docs-site-issues}[issue tracker].

=== Unscheduled

* [ ] Set up a roadmap page for Core components in the docs {uri-issues}/223[#223]
* [ ] Document the `page-` attributes {uri-issues}/177[#177]
* [ ] Explain how to create a partial page {uri-issues}/176[#176]
* [ ] Document sitemap features {uri-issues}/168[#168]
* [ ] Improve custom publish provider documentation {uri-issues}/164[#164]
* [ ] Document how to create user-defined page attributes
* [ ] Document redirect features
* [ ] Document stem functionality with common UI integration scenarios
* [ ] Document how to add MathJax integration to the UI
* [ ] Document how to integrate external Javascript files with the UI
* [ ] Document maintenance and bug fix priority policies on antora.org
* [ ] Document release schedule on project site
* [ ] Document list of environment variables as page (or as appropriate) in CLI module
* [ ] Make Get Antora a category landing page
* [ ] Set up a What's New? in the Docs
* [ ] Create community participation guidelines
* [ ] Add changelog system to Docs
* [ ] Add contributing guide to Docs
* [ ] Remove most documentation-type content from README and replace with links to the appropriate Docs pages
// https://gitlab.com/antora/antora/issues/206#note_63768866
* [ ] Partition the CLI options into two tables, general options and generate options

== Antora Default UI

For a detailed list of current development tasks, refer to the UI {uri-ui-issues}[issue tracker].

=== v1.0.0

*Release Timeframe*: Q3 2018

* [ ] Add client-side search (algolia docsearch) {uri-ui-issues}/44[#44]
* [ ] IE 11 fixes
* [ ] Cut stable release of default UI
* [ ] Support sample content for preview pages written in AsciiDoc
* [ ] Upgrade preview site sample content {uri-ui-issues}/20[#20]

=== Unscheduled

* [ ] Create task list SVGs {uri-ui-issues}/31[#31]
* [ ] Enable unordered list marker styles {uri-ui-issues}/26[#26]
* [ ] Extract all colors into CSS variables {uri-ui-issues}/18[#18]
* [ ] Upgrade build to Gulp 4
* [ ] Improve SVG options stability
* [ ] Allow SVGs to be embedded directly into Handlebars template

== Completed Releases

See the {uri-changelog}[CHANGELOG] for a summary of notable features, functionality, and bug fixes that have already been released.
